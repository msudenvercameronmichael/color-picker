package edu.msudenver.cs390H.colorfinder;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

public class MainActivity extends Activity {


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

        // Initialize the color picker and surface view



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


    /**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements NumberPicker.OnValueChangeListener {

        public NumberPicker redNumberPicker;
        public NumberPicker greenNumberPicker;
        public NumberPicker blueNumberPicker;
        public SurfaceView coloredView;

        private int redIntNumber;
        private int greenIntNumber;
        private int blueIntNumber;

        boolean recievedIntent = false;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);


			return rootView;
		}
        @Override
        public void onActivityCreated(Bundle b)
        {
            super.onActivityCreated(b);

            Intent intent = getActivity().getIntent();
            String action = intent.getAction();

            if(action.contentEquals("edu.msudenver.cs390H.colorfinder"))
            {
                CharSequence text =  "Use the color pickers to select your color, then tap the colored view to return back to your app.";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),text, duration);
                toast.show();

                recievedIntent = true;
            }


            redNumberPicker = (NumberPicker)getActivity().findViewById(R.id.redNumberPicker);
            redNumberPicker.setMaxValue(255);
            redNumberPicker.setMinValue(0);
            redNumberPicker.setOnValueChangedListener(this);


            greenNumberPicker = (NumberPicker)getActivity().findViewById(R.id.greenNumberPicker);
            greenNumberPicker.setMaxValue(255);
            greenNumberPicker.setMinValue(0);
            greenNumberPicker.setOnValueChangedListener(this);


            blueNumberPicker = (NumberPicker)getActivity().findViewById(R.id.blueNumberPicker);
            blueNumberPicker.setMaxValue(255);
            blueNumberPicker.setMinValue(0);
            blueNumberPicker.setOnValueChangedListener(this);


            coloredView = (SurfaceView)getActivity().findViewById(R.id.coloredView);
            coloredView.setBackgroundColor(Color.rgb(redNumberPicker.getValue(),this.greenNumberPicker.getValue(),blueNumberPicker.getValue()));

            coloredView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(recievedIntent)
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("color",Color.rgb(redNumberPicker.getValue(),greenNumberPicker.getValue(),blueNumberPicker.getValue()));
                        getActivity().setResult(RESULT_OK,returnIntent);
                        getActivity().finish();
                    }
                    return false;
                }
            });
        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

            redIntNumber = redNumberPicker.getValue();
            greenIntNumber = greenNumberPicker.getValue();
            blueIntNumber = blueNumberPicker.getValue();

            coloredView.setBackgroundColor(Color.rgb(redIntNumber, greenIntNumber, blueIntNumber));

        }


    }

}
